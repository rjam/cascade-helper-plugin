$(document).ready(function() { 

    MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

    let atoEditorInterface = document.getElementById('ajax-modal'); //ajax-modal

    // Function to parse component tree and modify dom text
    function showComponentInfo() {
      
      function crawlComponentContent() {
              
            /****** main function to loop any dropdown item ****/
            function loopDropdownItem(dropdownName) {
          
              $(`.form-group-collapsible-header div.title span:contains('${dropdownName}')`).each(function (index) {

                // :contains is not exact match, so make sure it's exact before proceeding
                if ($(this).text() == dropdownName) {
                  // console.log('Matched: ' + $(this).text() + ' / ' + dropdownName)
               

                    let componentContent = $(this).closest('.form-group-collapsible.multi-group').find('> .form-group-collapsible-body > .form-group-collapsible:not(.hide) .sd-field-type-WYSIWYG:not(.hide) > textarea.wysiwyg').text();

                    // console.log(componentContent);

                      let parsedHtml = new DOMParser().parseFromString(componentContent, 'text/html');
                      let parsedContent = parsedHtml.body.innerText;
                      let parsedWithTags = parsedHtml.body.innerHTML;

                      // console.log(parsedWithTags)
                      // check if parsedContent wysiwyg variable is blank (means no text, it's an image or other code). Set variable to code string
                      if (parsedContent == "") {
                        if (parsedWithTags.includes("img")) {
                          parsedContent = "<em>image</em>";
                        } else if (parsedWithTags.includes("youtube")) {
                          parsedContent = "<em>YouTube embed</em>";
                        } else if (parsedWithTags.includes("iframe")) {
                          parsedContent = "<em>iFrame embed</em>";
                        } else {
                          parsedContent = '';
                        }
                      } 

                      let parsedContentShort = parsedContent.substring(0, 40);

                      let previewPath = $(this); 
                      
                      // check if span already exists, else create it
                      if ($("span", previewPath).length && parsedContentShort != "") {
                        //console.log($("span", this));
                        $("span", previewPath).html(parsedContentShort);
                      } else if (parsedContentShort != "") {
                        //console.log('doesn\'t exist');
                        $(previewPath).append("&nbsp;&nbsp;<span style=\"font-size: 12px; font-weight: 400; color: #2b2b2b;background:rgb(242 169 0 / 15%);padding: 2px 6px; border-radius: 4px;\">" + parsedContentShort + "</span>")
                      }

                    } 
                    
                    // code that runs if dropdown label is not exact match
                    // else if ($(this).text() != dropdownName) {
                    //   console.log('Not Matched: ' + $(this).text() + ' / ' + dropdownName);
                    // }

                 });

              }

                /**** dropdown names and looping for creating previews (case sensitive) ****/
                let dropDownName = ["Component", "Column"];

                $(dropDownName).each(function(index, value) {
                  // console.log(value)
                  loopDropdownItem(value);
                  
                });
                

      }

      // wasn't previewing on modal open, delay helps
      setTimeout(function() {
        crawlComponentContent();
      },500)
      
    }

    let observer = new MutationObserver(function (mutations, observer) {
        //console.log(mutations, observer); // uncomment to see detection in console
        showComponentInfo();
    });

    // Observer settings
    function runObserver() {
      observer.observe(atoEditorInterface, {
        subtree: true,
        attributeFilter: ["data-draft-being-saved", "aria-expanded"] //"class or aria-label or aria-expanded or style"
      });

      setTimeout(function() {
        showComponentInfo();
      },500)
      
    }

    

    // Runs on page load. Check URL setting, URL (&action=edit), if ATO, and run observer if necessary
    chrome.storage.sync.get("previewComponents", ({ previewComponents }) => {
      if (previewComponents === true && $(".dropdown-menu .asset-link-label:contains('ATO')").length && window.location.href.indexOf("&action=edit") > -1) { 
        // check for body with class modal-open, then run observer
        let checkForModal = setInterval(function(){
          if ($( "body" ).hasClass( "modal-open" )) {
            //console.log('modal');
            runObserver();
            clearInterval(checkForModal);
          } // else {
          //   console.log('no modal');
          // }
        }, 1000);
        
        }
      });


      // Edit button listener
      $('#edit-link').click(function(){
        chrome.storage.sync.get("previewComponents", ({ previewComponents }) => {
          //console.log(previewComponents);
          // if ON
          if (previewComponents === true) { 
             if ($(".dropdown-menu .asset-link-label:contains('ATO')").length) {
              let modalOpen;

                // function to check if URL contains "&action=edit"
                // if true, run observer, if false, stop checking
                function isEditing() {
                  if (window.location.href.indexOf("&action=edit") > -1) {
                    modalOpen = true;
                    // console.log('modal window ' + modalOpen);
                    clearInterval(editCheck);
                    runObserver();
                  } else {
                    modalOpen = false;
                    setTimeout(stopInterval, 60000); // how long to wait until modal opens 1 min
                  }
                }

                function stopInterval() {
                  clearInterval(editCheck);
                  //console.log(`modal window ${modalOpen}. Stop checking.`);
                }

                // set interval to check url for "&action=edit"
                let editCheck = setInterval(isEditing, 1000);

                //console.log('Wait for modal and run');
            }
          } else if (previewComponents === false) {
            observer.disconnect();
            // console.log("don't run");
          }
        });
      });

      // run on page load. Check if reveal component turned on.
      chrome.storage.sync.get("whatComponent", ({ whatComponent }) => {
        if (whatComponent === true) {
          inspectAtoComponent();
        }
      });

      // function to populate inspector element
      function populateInspectorElement(component) {
        // console.log('component is: ' + component);
        let atoInspectorDialogWindow = document.querySelector("#atoInspectorDialogWindow");
        let shadow = atoInspectorDialogWindow.shadowRoot;
        let componentName = shadow.querySelector("#componentName");
        if (component != null) {
         componentName.innerHTML = component;
        }
      }


    // function to create inspector element in shadow dom
    function createInspectorElement() {
      // $("body").after('<div id="atoShadow"></div>');

      let el = document.createElement('div');
      el.id = 'atoInspectorDialogWindow';
      el.setAttribute('style', 'all: initial !important;');
      // el.setAttribute('id', 'atoInspectorDialogWindow');

      document.documentElement.appendChild(el);

      // let el = document.querySelector("body");
      el.attachShadow({mode: "open"});
      // Just like prototype & constructor bi-directional references, we have...
      el.shadowRoot // the shadow root.
      el.shadowRoot.host // the element itself.

      el.shadowRoot.innerHTML = `<link rel="stylesheet" type="text/css" href="${chrome.runtime.getURL("inspector.css")}">`;

      // add html
      let wrapper = document.createElement("div");
      wrapper.id = "wrapper"
      wrapper.innerHTML = `<span id="closeBtn">Dismiss</span><p>You're hovering over <span id="componentName">...</span></p><p id="helpText">Hello Editor! This is the Cascade<span style="font-weight:600">Helper</span> Chrome extension. Only you can see this notification. To disable this feature turn off "Component Identifier" in the extension settings.</p>`;
      el.shadowRoot.appendChild(wrapper);


      // close button functionality
      let atoInspectorDialogWindow = document.querySelector("#atoInspectorDialogWindow");
      let shadow = atoInspectorDialogWindow.shadowRoot;
      shadow.querySelector("#closeBtn").addEventListener("click", function(){
        atoInspectorDialogWindow.remove();
        // this.parentNode.remove();
      });
      

      // console.log('function to create shadow dom element ran');
    }


    // function to get ATO component
    function inspectAtoComponent() {

          const classExists = document.getElementsByClassName(
            'ato__components'
          ).length > 0;

        // verify if on ATO page by checking .ato__components class
        if (classExists) {
          // console.log('✅ ATO class exists on page');

          createInspectorElement(); // create shadow dom element

          $( '.ato__components > div.ato-component' ).mouseenter( function(e) {
            
            
          let idOfElementOnHover = e.currentTarget.id;
          let classOfElementOnHover = e.currentTarget.className;
          

          let components = [{
            id: 'accordions',
            class: null,
            prettyName: 'Accordions'
            }, {
            id: 'breadcrumbs',
            prettyName: 'Breadcrumbs'
            }, {
              id: 'events',
              prettyName: 'Events'
              }, {
                id: 'filter',
                prettyName: 'Filter'
              }, {
                id: 'full_width_banner',
                prettyName: 'Full-Width Banner'
              }, {
                id: 'full_width_image_links',
                prettyName: 'Full-Width Image Links'
              }, {
                id: 'fw_video',
                prettyName: 'Full Width Video'
              }, {
                id: 'iframe',
                prettyName: 'iFrame'
              }, {
                id: 'leftnav',
                prettyName: 'Left Nav'
              }, {
                id: 'multicolumn',
                prettyName: 'Multicomponent'
              }, {
                id: 'news',
                prettyName: 'News'
              }, {
                id: 'number_counter',
                prettyName: 'Number Counter'
              }, {
                id: 'pathways',
                prettyName: 'Pathways'
              }, {
                id: 'quotes',
                prettyName: 'Quotes'
              }, {
                id: 'search_results',
                prettyName: 'Search Results'
              }, {
                id: 'registrar_report',
                prettyName: 'Registrar Report'
              }, {
                id: 'story_selector',
                prettyName: 'Story Selector'
              }, {
                id: 'text_block',
                prettyName: 'Text Block'
              }, {
                id: 'wyntk',
                prettyName: 'What You Need to Know'
              }, {
                id: null,
                class: 'spolight__gallery',
                prettyName: 'Spotlight Gallery'
              }
          ];

            // check for match with component's id or class
            for (let i = 0; i < components.length; i++ ) {
              if (idOfElementOnHover.includes(components[i].id) || classOfElementOnHover.includes(components[i].class)) {
                // console.log(components[i].prettyName);
                populateInspectorElement(components[i].prettyName);
              }
            }
            
            $( 'header' ).mouseenter( function(e) {
              populateInspectorElement('page header');
            });
            $( 'footer' ).mouseenter( function(e) {
              populateInspectorElement('page footer');
            });
          
          } 
          
        );
          
        } else {
          // console.log('⛔️ ATO class does NOT exist on page');
        }
    }

    function stopInspectAtoComponent() {
      $( '.ato__components > div' ).unbind('mouseenter mouseleave');
      
      // remove shadow dom element
      let atoInspectorDialogWindow = document.querySelector("#atoInspectorDialogWindow");
      atoInspectorDialogWindow.remove();

    }

    


    // Popup setting listener for ATO component content preview
    chrome.runtime.onMessage.addListener(function (request) {
      if (request.action === 'runObserver') {
        // runs if ATO & direct 'edit URL' is loaded
        if ($(".dropdown-menu .asset-link-label:contains('ATO')").length && window.location.href.indexOf("&action=edit") > -1) {
          runObserver();
        }
      }
      if (request.action === 'stopObserver') {
          observer.disconnect();
      }
    });



   // Popup setting to detect if component ID is turned on or off
    chrome.runtime.onMessage.addListener(function (request) {
      if (request.action === 'startComponentId') {
        inspectAtoComponent();
      } else if (request.action === 'stopComponentId') {
        // console.log('no need to inspect');
        stopInspectAtoComponent();
      }

    });

    
    

  
    
   

});



