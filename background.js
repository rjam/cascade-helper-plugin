// https://developer.chrome.com/docs/extensions/mv3/getstarted/
// default to show ATO component previews
let previewComponents;

// determine on/off ATO Content Preview setting on install or update
chrome.runtime.onInstalled.addListener(() => {
  chrome.storage.sync.get("previewComponents", ({ previewComponents }) => {
    if (previewComponents == null) {
      chrome.storage.sync.set({ previewComponents: true }); // if not set, set to true
    }
    //console.log(previewComponents);
  });

  chrome.storage.sync.get("whatComponent", ({ whatComponent }) => {
    if (whatComponent == null) {
      chrome.storage.sync.set({ whatComponent: false }); // if not set, set to false
    }
  });

});