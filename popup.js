// get component content reveal checkbox value from storage and set checkbox accordingly
chrome.storage.sync.get("previewComponents", ({ previewComponents }) => {
    if (previewComponents === true) {
        //console.log('previewComponents: true (from popup script)')
        document.getElementById("checkbox").checked = true; // set popup to checked
    } else if (previewComponents === false) {
        //console.log('previewComponents: false (from popup script)')
        document.getElementById("checkbox").checked = false; // set popup to unchecked
    }
  });

// get component id checkbox value from storage and set checkbox accordingly
  chrome.storage.sync.get("whatComponent", ({ whatComponent }) => {
    if (whatComponent === true) {
        document.getElementById("checkboxComponentId").checked = true; // set popup to checked
    } else if (whatComponent === false) {
        document.getElementById("checkboxComponentId").checked = false; // set popup to unchecked
    }
});

  
// on change, save checkbox value to local storage
$("#checkbox").off('click').on('click', function () {
    if (this.checked) {
        chrome.storage.sync.set({ previewComponents: true }); // update setting in storage
        $('#alertMessage span').hide().html('Preview ON').fadeIn().delay(1000).fadeOut();
        chrome.tabs.query({ active: true, currentWindow: true }, function (activeTabs) {
            chrome.tabs.sendMessage(activeTabs[0].id, { action: 'runObserver' });
        });
    } else {
        chrome.storage.sync.set({ previewComponents: false }); // update setting in storage
        $('#alertMessage span').hide().html('Preview OFF: refresh to see changes').fadeIn().delay(1000).fadeOut();
        chrome.tabs.query({ active: true, currentWindow: true }, function (activeTabs) {
            chrome.tabs.sendMessage(activeTabs[0].id, { action: 'stopObserver' });
        });
    }
});


// on change, save component ID checkbox value to local storage
$("#checkboxComponentId").off('click').on('click', function () {
    if (this.checked) {
        chrome.storage.sync.set({ whatComponent: true }); // update setting in storage
        $('#alertMessage span').hide().html('Activated').fadeIn().delay(1000).fadeOut();
        chrome.tabs.query({ active: true, currentWindow: true }, function (activeTabs) {
            chrome.tabs.sendMessage(activeTabs[0].id, { action: 'startComponentId' });
        });
    } else {
        chrome.storage.sync.set({ whatComponent: false }); // update setting in storage
        $('#alertMessage span').hide().html('Deactivated').fadeIn().delay(1000).fadeOut();
        chrome.tabs.query({ active: true, currentWindow: true }, function (activeTabs) {
            chrome.tabs.sendMessage(activeTabs[0].id, { action: 'stopComponentId' });
        });
    }
});



/* copy colours to clipboard script */
function copyToClipboard(bgColor) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(bgColor).select();
    document.execCommand("copy");
    $temp.remove();
}

var colourClasses = ".singleColour.purple, .singleColour.gold, .singleColour.red, .singleColour.mauve, .singleColour.orange, .singleColour.blue, .singleColour.green";

$(colourClasses).click(function (e) {
    var bgColor = $(e.target).attr('data-color');
    var bgColor = bgColor.toString();
    console.log(bgColor);
    copyToClipboard(bgColor);
});

// button with two classes doesn't keep focus on click. this forces it to keep focus (for 'copied' pseudo css)
$(".singleColour").click(function () {
    $this = $(this);
    $this.focus();
    // remove focus after time
    setTimeout(function () {
        $this.blur();
    }, 2000);
});

